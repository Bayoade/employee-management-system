﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using EmployeeManagementSystem.Core.Interfaces.Repository;

namespace EmployeeManagementSystem.Data.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public readonly EMSystemDbContext _dbContext;
        private DbSet<T> table = null;

        public GenericRepository(EMSystemDbContext dbContext)
        {
            _dbContext = dbContext;
            table = _dbContext.Set<T>();

        }
        public void CreateRange(IList<T> model)
        {
            table.AddRangeAsync(model);
            _dbContext.SaveChanges();
        }

        public void Delete(params object[] ids)
        {
            var items = table.Find(ids);

            table.RemoveRange(items);

            _dbContext.SaveChanges();
        }

        public async Task<T> Get(Expression<Func<T, bool>> where)
        {
            return await _dbContext.Set<T>().FirstOrDefaultAsync(where);
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> where)
        {
            return _dbContext.Set<T>().Where(where);
        }

        public T GetById(object id)
        {
            return table.Find(id);
        }

        public IQueryable<T> Query()
        {
            return _dbContext.Set<T>();
        }
    }
}
