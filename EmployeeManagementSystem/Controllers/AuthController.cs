﻿using EmployeeManagementSystem.Core.Entities;
using EmployeeManagementSystem.Core.Entities.Enums;
using EmployeeManagementSystem.Core.Helpers;
using EmployeeManagementSystem.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;    
        private readonly RoleManager<ApplicationUserRole> _userRoleManager;
        private readonly SignInManager<ApplicationUser>  _signInManager;
        private readonly IConfiguration _configuration;

        public AuthController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationUserRole> userRoleManager, 
            RoleManager<ApplicationRole> roleManager, 
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _userRoleManager = userRoleManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    //Check his role and then route the user to the right part of the application

                    return LocalRedirect(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View();
                }
            }

            // If we got this far, something failed, redisplay form
            return View();

        }

        public IActionResult Register()
        {
            return View();
        }
        
        public async Task<IActionResult> Register(LoginViewModel model)
        {
            var user = new ApplicationUser { UserName = model.UserName, Email = model.UserName };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                var superUser = await _userManager.FindByNameAsync(_configuration.GetSection("SuperUserEmail").ToString());

                if (!superUser.IsNull())
                {
                    var role = await _roleManager.FindByNameAsync(RoleType.Admin.ToString());

                    var userRole = await _userRoleManager.CreateAsync(new ApplicationUserRole
                    {
                        UserId = user.Id,
                        RoleId = role.Id
                    });

                    if (userRole.Succeeded)
                    {
                        return View(new Response
                        {
                            Status = true,
                            Message = "Successful",
                            Data = null
                        });
                    }

                    return View(new Response
                    {
                        Status = false,
                        Message = "Failed",
                        Data = null
                    });
                }

                var roleUser = await _roleManager.FindByNameAsync(RoleType.User.ToString());

                var userRoleIdentityResult = await _userRoleManager.CreateAsync(new ApplicationUserRole
                {
                    UserId = user.Id,
                    RoleId = roleUser.Id
                });

                if (userRoleIdentityResult.Succeeded)
                {
                    return View(new Response
                    {
                        Status = true,
                        Message = "Successful",
                        Data = null
                    });
                }

                return View(new Response
                {
                    Status = false,
                    Message = "Failed",
                    Data = null
                });



            }

            return View(new Response
            {
                Status = false,
                Message = "Failed",
                Data = null
            });
        }

        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View();
        }
    }
}
