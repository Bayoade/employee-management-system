﻿using EmployeeManagementSystem.Core.Entities;
using EmployeeManagementSystem.Core.Interfaces.Repository;
using EmployeeManagementSystem.Core.Interfaces.Service;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Infrastructure.Services
{
    public class EmployeeService : IEmployeeService
    {
        public readonly IGenericRepository<Employee> _genericRepository;
        public EmployeeService(IGenericRepository<Employee> genericRepository)
        {
            _genericRepository = genericRepository;
        }
        public Guid CreateEmployee(Employee model)
        {
            var id = model.Id = Guid.NewGuid();
            model.CreatedDate = DateTime.Now;

             _genericRepository.CreateRange(new List<Employee>
            {
                model
            });

            return id;
        }

        public async Task<List<Employee>> GetAllEmployee()
        {
            return await _genericRepository.GetAll(x => x.IsActive).ToListAsync();
        }

        public bool DeleteEmployeeByIds(params Guid[] ids)
        {
            _genericRepository.Delete(ids);

            return true;
        }

        public Employee GetEmployee(Guid id)
        {
            return _genericRepository.GetById(id);
        }
    }
}
