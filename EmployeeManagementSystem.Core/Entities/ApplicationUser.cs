﻿using Microsoft.AspNetCore.Identity;
using System;

namespace EmployeeManagementSystem.Core.Entities
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}
