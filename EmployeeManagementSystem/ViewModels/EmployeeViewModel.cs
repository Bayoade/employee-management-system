﻿using System;

namespace EmployeeManagementSystem.ViewModels
{
    public class EmployeeViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public DateTime DOB { get; set; }
        public string Department { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
