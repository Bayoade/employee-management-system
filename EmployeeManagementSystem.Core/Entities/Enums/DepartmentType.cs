﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeManagementSystem.Core.Entities.Enums
{
    public enum  DepartmentType
    {
        [Display(Name = "Administration")]
        Administrator,

        [Display(Name = "CEO")]
        CEO,

        [Display(Name = "Management")]

        Management,

        [Display(Name = "Finance")]
        Finance,

        [Display(Name = "Human Resource")]
        HR,

        [Display(Name = "Operations")]
        Operations,

        [Display(Name = "Marketing")]
        Marketing

    }
}
