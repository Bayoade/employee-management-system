﻿using EmployeeManagementSystem.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Core.Interfaces.Service
{
    public interface IEmployeeService
    {
        Guid CreateEmployee(Employee model);
        Employee GetEmployee(Guid id);
        Task<List<Employee>> GetAllEmployee();
        bool DeleteEmployeeByIds(params Guid[] ids);
    }
}
