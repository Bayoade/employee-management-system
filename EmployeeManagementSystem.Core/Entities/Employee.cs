﻿using EmployeeManagementSystem.Core.Entities.Enums;
using System;

namespace EmployeeManagementSystem.Core.Entities
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public DateTime DOB { get; set; }
        public DepartmentType Department {get; set;}
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
