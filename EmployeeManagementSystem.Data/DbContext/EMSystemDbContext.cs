﻿using EmployeeManagementSystem.Core.Entities;
using EmployeeManagementSystem.Core.Entities.Enums;
using Microsoft.EntityFrameworkCore;
using System;

namespace EmployeeManagementSystem.Data
{
    public class EMSystemDbContext : DbContext
    {
        public EMSystemDbContext(DbContextOptions<EMSystemDbContext> options)
            : base(options)
        {
        }

        internal DbSet<ApplicationUser> Users { get; set; }
        internal DbSet<ApplicationRole> Roles { get; set; }
        internal DbSet<ApplicationUserRole> UserRoles { get; set; }
        internal DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            var employeeModelBuilder = modelBuilder.Entity<Employee>();

            employeeModelBuilder.HasKey(x => x.Id);

            modelBuilder.Entity<ApplicationRole>().HasData(
                new ApplicationRole { Id = Guid.Parse("8B974C47-18EC-4977-9E7E-F59381224948"), Name = RoleType.Admin.ToString(), NormalizedName = DepartmentType.Administrator.ToString().ToUpper(), ConcurrencyStamp = Guid.NewGuid().ToString() },
                new ApplicationRole { Id = Guid.Parse("1252E35F-9616-4DC2-8E3C-F30512D2C9E5"), Name = RoleType.User.ToString(), NormalizedName = DepartmentType.Management.ToString().ToUpper(), ConcurrencyStamp = Guid.NewGuid().ToString() }            );
        }
    }
}
