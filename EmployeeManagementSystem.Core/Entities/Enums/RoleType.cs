﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeManagementSystem.Core.Entities.Enums
{
    public enum RoleType
    {
        [Display(Name = "Administrator")]
        Admin,

        [Display(Name = "User")]
        User,
    }
}
