﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Core.Interfaces.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        void CreateRange(IList<T> model);
        void Delete(params object[] ids);
        Task<T> Get(Expression<Func<T, bool>> where);
        IQueryable<T> Query();
        IQueryable<T> GetAll(Expression<Func<T, bool>> where);
        T GetById(object id);
    }
}
