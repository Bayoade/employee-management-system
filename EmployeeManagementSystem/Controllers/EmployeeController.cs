﻿using EmployeeManagementSystem.Core.Entities;
using EmployeeManagementSystem.Core.Interfaces.Service;
using EmployeeManagementSystem.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        public async Task<IActionResult> Index()
        {
            var employees = await _employeeService.GetAllEmployee();
            var employeeVM = new List<EmployeeViewModel>();

            employees.ForEach(employee =>
            {
                employeeVM.Add(new EmployeeViewModel
                {
                    Id = employee.Id,
                    CreatedDate = employee.CreatedDate,
                    Department = employee.Department.ToString(),
                    DOB = employee.DOB,
                    FirstName = employee.FirstName,
                    IsActive = employee.IsActive,
                    LastName = employee.LastName
                });
            });

            return View(employeeVM);
        }

        public IActionResult AddEmployee(SaveEmployeeViewModel model)
        {
            var employee = new Employee
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Department = model.Department,
                DOB = model.DOB,
                IsActive = model.IsActive
            };

            var newId = _employeeService.CreateEmployee(employee);

            return View(newId);
        }

        public IActionResult GetAllEmployeeId(Guid id)
        {
            var employee= _employeeService.GetEmployee(id);
            var employeeVM = new EmployeeViewModel
            {
                Id = employee.Id,
                CreatedDate = employee.CreatedDate,
                Department = employee.Department.ToString(),
                DOB = employee.DOB,
                FirstName = employee.FirstName,
                IsActive = employee.IsActive,
                LastName = employee.LastName
            };

            return View(employeeVM);
        }

        public IActionResult DeleteEmployeeId(Guid id)
        {
            var isDeleted = _employeeService.DeleteEmployeeByIds(id);

            return View(isDeleted);
        }
    }
}
